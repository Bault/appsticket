import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';

import {AddPage} from '../pages/add/add';
import {SearchPage} from '../pages/search/search';
import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {MyContentComponent} from '../components/my-content/my-content';
import {ConnectPage} from "../pages/connect/connect";
import {HttpModule} from "@angular/http";
import {ProductPage} from "../pages/product/product";
import {Camera} from "@ionic-native/camera";
import {Facebook} from "@ionic-native/facebook";
import {LocalNotifications} from "@ionic-native/local-notifications";

@NgModule({
    declarations: [
        MyApp,
        AddPage,
        SearchPage,
        HomePage,
        TabsPage,
        ConnectPage,
        ProductPage,
        MyContentComponent
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        HttpModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        AddPage,
        SearchPage,
        HomePage,
        TabsPage,
        ProductPage,
        ConnectPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Camera,
        Facebook,
        LocalNotifications,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}
