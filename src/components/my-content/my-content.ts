import {Component, EventEmitter, Input, Output} from '@angular/core';

/**
 * Generated class for the MyContentComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
    selector: 'my-content,[my-content]',
    templateUrl: 'my-content.html'
})
export class MyContentComponent {
    @Input('my-content') title: String;
    @Input() enable: Boolean = false;
    @Output() eventDoRefresh = new EventEmitter<any>();

    doRefresh(event) {
        this.eventDoRefresh.emit(event);
    }

    constructor() {
    }

}
