import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyContentComponent } from './my-content';

@NgModule({
  declarations: [
    MyContentComponent,
  ],
  imports: [
    IonicPageModule.forChild(MyContentComponent),
  ],
  exports: [
    MyContentComponent
  ]
})
export class MyContentComponentModule {}
