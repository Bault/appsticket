import {Injectable} from "@angular/core";
import {ToastController} from "ionic-angular";
/**
 * Created by petit on 02/06/2017.
 */

@Injectable()
export class ToolsService{

    constructor(public toastCtrl:ToastController) {
    }

    showToast(message,cssClass){
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            cssClass: cssClass,
            showCloseButton:true
        });
        toast.present();
    }

}