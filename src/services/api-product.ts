import {Http} from "@angular/http";
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/toPromise';
import {Product} from "../models/product";
/**
 * Created by petit on 02/06/2017.
 */

@Injectable()
export class ApiProductService{
    // private apiUrl = 'http://localhost:3000/products';
    private apiUrl = 'http://192.168.2.31:3000/products';

    constructor(private http: Http){
    }

    getProducts(): Promise<Product[]> {
        return this.http.get(this.apiUrl)
            .toPromise()
            .then(response =>
                response.json() as Product[])
            .catch(this.handleError);
    }

    getProduct(obj): Promise<Product> {
        const apiUrlId = `${this.apiUrl}/${obj.id}`;
        return this.http.get(apiUrlId)
            .toPromise()
            .then(response =>
                response.json() as Product)
            .catch(this.handleError);
    }

    deleteProduct(obj): Promise<Product[]> {
        const apiUrlId = `${this.apiUrl}/${obj.id}`;
        return this.http.delete(apiUrlId)
            .toPromise()
            .then(response =>
                response.json() as Product[])
            .catch(this.handleError);
    }

    addProduct(obj): Promise<Product> {
        return this.http.post(this.apiUrl,obj)
            .toPromise()
            .then(response =>
                response.json() as Product)
            .catch(this.handleError);
    }

    updateProduct(obj): Promise<Product> {
        const apiUrlId = `${this.apiUrl}/${obj.id}`;
        return this.http.put(apiUrlId,obj)
            .toPromise()
            .then(response =>
                response.json() as Product)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}