import {Http} from "@angular/http";
import {User} from "../models/user";
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/toPromise';
/**
 * Created by petit on 02/06/2017.
 */

@Injectable()
export class ApiUserService{
    // private apiUrl = 'http://localhost:3000/users';
    private apiUrl = 'http://192.168.2.31:3000/users';

    constructor(private http: Http){
    }

    getUsers(): Promise<User[]> {
        return this.http.get(this.apiUrl)
            .toPromise()
            .then(response =>
                response.json() as User[])
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}