/**
 * Created by petit on 01/06/2017.
 */

export class User {
    id:number;
    username:string;
    password:string;
    firstName:string;
    lastName:string;
    facebookID:number;

    constructor(id: number, username: string, password: string, firstName: string, lastName: string, facebookID?: number) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.facebookID = facebookID;
    }
}