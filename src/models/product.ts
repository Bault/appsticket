/**
 * Created by petit on 01/06/2017.
 */

export class Product {
    id:number;
    brand:string;
    name:string;
    dateBuy:string;
    expiration:string;
    picture:string;


    constructor(id?: number, brand?: string, name?: string, dateBuy?: string, expiration?: string, picture?: string) {
        this.id = id;
        this.brand = brand;
        this.name = name;
        this.dateBuy = dateBuy;
        this.expiration = expiration;
        this.picture = picture;
    }
}