import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Product} from "../../models/product";
import {ApiProductService} from "../../services/api-product";
import {ToolsService} from "../../services/tools";
import {HomePage} from "../home/home";
import {LocalNotifications} from "@ionic-native/local-notifications";
import * as moment from 'moment';

@Component({
    selector: 'page-add',
    templateUrl: 'add.html',
    providers:[ApiProductService,ToolsService]
})
export class AddPage {

    productToAdd: Product = new Product();

    constructor(
        public navCtrl: NavController,
        public apiProductService: ApiProductService,
        public toolsService:ToolsService,
        public localNotifications: LocalNotifications
    ) {

    }
 
    addProduct(){
        let self = this;
        let addNotification = (product)=>{
            let dateNotif = moment(product.dateBuy,"DD/MM/YYY").add(product.expiration,'M').toDate();
            self.localNotifications.schedule({
                id: product.id,
                text: 'Le produit '+product.name+' arrive bientôt à expiration.',
                at: 999999999999
                // at: new Date(new Date().getTime()+3000*12)
                // at: dateNotif
            });
        };
        this.apiProductService.addProduct(this.productToAdd)
            .then(
                (data)=>{
                    addNotification(data);
                    this.toolsService.showToast("Ajout du produit réussi, vous allez être redirigé dans quelques instants.","toast primary");
                    this.productToAdd = new Product();
                    setTimeout(()=>{
                        this.navCtrl.push(HomePage,{},{
                            animate:true
                        });
                    },3000);
                }
            )
            .catch(
                (err)=>{
                    this.toolsService.showToast(err,"toast danger");
                }
            )
    }
}
