import {Component} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the ProductPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
    selector: 'page-product',
    templateUrl: 'product.html'
})
export class ProductPage {
    pageData;

    constructor(public viewCtrl: ViewController, public params: NavParams) {
        this.pageData = this.params.get('product');
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

}
