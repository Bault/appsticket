import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {ApiProductService} from "../../services/api-product";
import {Product} from "../../models/product";
import {ToolsService} from "../../services/tools";
import { ModalController } from 'ionic-angular';
import {ProductPage} from "../product/product";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {LocalNotifications} from "@ionic-native/local-notifications";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
    providers:[ApiProductService,ToolsService]
})
export class HomePage {

    products:Array<Product> = [];

    constructor(
        public navCtrl: NavController,
        public apiProductService: ApiProductService,
        public toolsService:ToolsService,
        public modalCtrl: ModalController,
        public camera: Camera,
        public localNotifications: LocalNotifications
    )
    {
    }

    options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
    };

    loadData(refresher?){
        this.apiProductService.getProducts()
            .then(
                (data:Product[])=>{
                    //pour debug des notifs uniquement
                    this.localNotifications.getAll().then(
                        (data)=>{console.log(data)}
                    ).catch(
                        (err)=>{console.log(err)}
                    );

                    this.products = data;
                    if(refresher){
                        refresher.complete();
                    }
                }
            )
            .catch(
                (err)=>{
                    this.toolsService.showToast(err,"toast danger");
                    if(refresher){
                        refresher.cancel();
                    }
                }
            )
    }

    ionViewDidEnter(){
        this.loadData()
    }

    doRefresh(refresher){
        this.loadData(refresher);
    }

    deleteProduct(obj){
        this.apiProductService.deleteProduct(obj)
            .then(
                ()=>{
                    let index = this.products.findIndex(x => x.id==obj.id);
                    this.products.splice(index,1);
                    this.localNotifications.clear(obj.id);
                }
            )
            .catch(
                (err)=>{
                    this.toolsService.showToast(err,"toast danger");
                }
            )
    }

    showProduct(obj){
        this.apiProductService.getProduct(obj)
            .then(
                (data:Product)=>{
                    this.presentModal(obj);
                }
            )
            .catch(
                (err)=>{
                    this.toolsService.showToast(err,"toast danger");
                }
            )
    }

    presentModal(obj) {
        let modal = this.modalCtrl.create(ProductPage, {product:obj});
        modal.present();
    }

    addPicture(obj){
        this.camera.getPicture(this.options).then((imageData) => {
            obj.picture = 'data:image/jpeg;base64,' + imageData;
            this.apiProductService.updateProduct(obj)
                .then(
                    (data)=>{
                        this.toolsService.showToast("Ajout de la photo réussi.","toast primary");
                    }
                )
                .catch(
                    (err)=>{
                        this.toolsService.showToast(err,"toast danger");
                    }
                )
        },(err) => {
            this.toolsService.showToast(err,"toast danger");
        });
    }

}
