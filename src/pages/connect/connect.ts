import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {User} from "../../models/user";
import {TabsPage} from "../tabs/tabs";
import {ApiUserService} from "../../services/api-user";
import {ToolsService} from "../../services/tools";
import {Facebook, FacebookLoginResponse} from "@ionic-native/facebook";

/**
 * Generated class for the ConnectPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-connect',
    templateUrl: 'connect.html',
    providers:[ApiUserService,ToolsService]
})
export class ConnectPage {

    username: String;
    password: String;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiUserService:ApiUserService,
        public toolsService:ToolsService,
        private fb:Facebook
    )
    {
    }

    logUser() {
        this.apiUserService.getUsers()
            .then(
                (data: User[])=> {
                    let ret = data.filter(item => item.username === this.username && item.password === this.password);
                    if (ret.length > 0) {
                        this.navCtrl.setRoot(TabsPage,{},{
                            animate:true
                        });
                    } else {
                        this.toolsService.showToast("Aucun compte correspond, merci de réessayer avec d'autres identifiants.","toast danger");
                    }
                }
            )
            .catch(
                (err)=> {
                    this.toolsService.showToast(err,"toast danger");
                }
            );
    }

    logFb() {
        let self = this;

        let connectWithFb = (response) => {
            self.apiUserService.getUsers()
                .then(
                    (data: User[])=> {
                        let ret = data.filter(item => item.facebookID === response.authResponse.userID);
                        if (ret.length > 0) {
                            self.navCtrl.setRoot(TabsPage,{},{
                                animate:true
                            });
                        } else {
                            self.toolsService.showToast("Aucun compte correspond, merci de réessayer avec d'autres identifiants.","toast danger");
                        }
                    }
                )
                .catch(
                    (err)=> {
                        self.toolsService.showToast(err,"toast danger");
                    }
                );
        };

        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(connectWithFb)
            .catch(e => console.log('Error logging into Facebook', e));

    }

}
